.PHONY: help
help:
	@cat Makefile

public: ./mkdocs.yml $(wildcard docs/*.md)
	mkdocs build --clean

